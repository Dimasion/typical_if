// |----------------------------------------------------|
// |----------------- MENU FUNCTIONS  ------------------|
// |----------------------------------------------------|

// |TOGGLE MENU ----------------------------------------|
function menuToggle() {
    $(nav).toggleClass(navIsOpened);
    $(swipeWrap).toggleClass(swipeWrapIsActive);
    if ($(overlay).hasClass(overlayIsActive)) {
        $(overlay).removeClass(overlayIsActive);
    } else {
        $(overlay).addClass(overlayIsActive);
    }
}

// |OPEN MENU -----------------------------------------|
function menuOpen() {
    $(nav).addClass(navIsOpened);
    $(swipeWrap).addClass(swipeWrapIsActive);
    $(overlay).addClass(overlayIsActive);
}

// |CLOSE MENU -----------------------------------------|
function menuClose() {
    $(nav).removeClass(navIsOpened);
    $(swipeWrap).removeClass(swipeWrapIsActive);
    $(overlay).removeClass(overlayIsActive, overlayIsActiveUp);
}

// |----------------------------------------------------|
// |------------------- MENU EVENTS --------------------|
// |----------------------------------------------------|

// |NAV MENU BTN CLICK (HAMBURGER) ---------------------|
$(navBtn).click(function() {
    moveSearch();
    menuToggle();

});

// |NAVIGATION HOVER (>=1200PX) ------------------------|
if (sizeWindow >= screenMedia) {
    $(nav).hover(function() {
            menuOpen();
        },
        function() {
            menuClose();
        });
}

// |----------------------------------------------------|
// |------------------  SWIPE EVENTS  ------------------|
// |----------------------------------------------------|


if (sizeWindow < screenMedia) {
    // |SWIPE LEFT -------------------------------------|
    $(swipeWrap).on("swipeleft", function() {
        closeSearch();
        menuClose();
    });

    // |SWIPE RIGHT ------------------------------------|
    $(swipeWrap).on("swiperight", function() {
        if (!$(search).hasClass(searchIsClosed)) {
            closeSearch();
            setTimeout(function() {
                menuOpen();
                moveSearch();
            }, transNormal);
        } else {
            $(search).removeClass(searchIsMoved);
            menuOpen();
            moveSearch();
        }
    });
}
