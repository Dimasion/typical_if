// |----------------------------------------------------|
// |--------------  CONNECT SIMPLEWEATHER.JS  ----------|
// |----------------------------------------------------|

$(document).ready(function() {
    $.simpleWeather({
        location: 'Ivano-Frankivsk, UA',
        woeid: '',
        unit: 'c',
        success: function(weather) {
            html = '<h2><i class="weather-logo icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2>';
            $("#weather").html(html);
        },
        error: function(error) {
            $("#weather").html('<p>' + error + '</p>');
        }
    });
});
