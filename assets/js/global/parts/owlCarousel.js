$(document).ready(function() {

  $(".news-item__slider").owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
      lazyEffect : 'fade',
      addClassActive : true
  });

});
