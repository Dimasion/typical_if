// |----------------------------------------------------|
// |----------------- COMMON CONNECTS ------------------|
// |----------------------------------------------------|
//Connect SVG crossbrowser
svg4everybody();

//remove Transitions on page load
$(window).load(function() {
    $("body").removeClass("preload");
});

// |----------------------------------------------------|
// |----------------- DOCUMENT OPTIONS -----------------|
// |----------------------------------------------------|
var sizeWindow = $(window).width(),
    transNormal = 250,
    transVFast = 150;

// |----------------------------------------------------|
// |----------------- MEDIA OPTIONS --------------------|
// |----------------------------------------------------|
var screenMedia = 1180,
    desktopMedia = 972,
    tabletsMedia = 748,
    phoneMedia = 460,
    iPhoneMedia = 300;

// |----------------------------------------------------|
// |----------------- OVERLAY OPTIONS ------------------|
// |----------------------------------------------------|
var overlay = $('.js-overlay');
//OVERLAY MODIFIERS
var overlayIsActive = 'overlay--behind',
    overlayIsActiveUp = 'overlay--in-front';

// |----------------------------------------------------|
// |----------------- SWIPE WRAP   ---------------------|
// |----------------------------------------------------|
var swipeWrap = $('.js-swipe-wrap');


//SWIPE MODIFIERS
var swipeWrapIsActive = 'menu-swipe-wrap--active';

// |----------------------------------------------------|
// |----------------- MENU OPTIONS ---------------------|
// |----------------------------------------------------|
var nav = $('.js-nav'),
    navBtn = $('.js-nav-btn'),
    navItem = $('.js-menu-item'),
    navCategory = $('.js-menu-category');

//MENU MODIFIERS
var navIsOpened = 'nav-main--opened',
    menuItemHasHover = 'menu__item--hover';

// |----------------------------------------------------|
// |----------------- SEARCH OPTIONS -------------------|
// |----------------------------------------------------|
var search = $('.js-search'),
    searchForm = $('.js-search-form'),
    searchInput = $('.js-search-input');

//SEARCH MODIFIERS
var searchIsMoved = 'search--moved',
    searchIsClosed = 'search--closed';


// |----------------------------------------------------|
// |----------------- FOOTER OPTIONS  ------------------|
// |----------------------------------------------------|

var socBox = $('.js-social'),
    socTextBox = $('.js-social-text-box'),
    socText = $('.js-social-text'),
    socVk = $('.js-social-vk'),
    socFb = $('.js-social-fb'),

    //MODIFIERS
    socTextBoxHover = 'social__text-box--hover',
    socVkMoved = 'social__vk--moved',
    socFbMoved = 'social__fb--moved';


// |----------------------------------------------------|
// |----------------- COMMENTS OPTIONS------------------|
// |----------------------------------------------------|
var likePlus = $('.like-plus'),
    likeMinus = $('.like-minus'),
    likeCount = $('.like-count'),
    likeIcon = $('.like-icon'),
    auth = $('.auth'),
    commentTextArea = $('.comment__textarea'),

    //MODIFIERS
    likeIsPossitive = 'like-icon--possitive',
    likeIsNegative = 'like-icon--negative';

var socBoxModal = $('.js-social--modal'),
    socTextBoxModal = $('.js-social-text-box--modal'),
    socTextModal = $('.js-social-text--modal'),
    socVkModal = $('.js-social-vk--modal'),
    socFbModal = $('.js-social-fb--modal');
