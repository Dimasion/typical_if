//LATEST NEWS

$('.latest-news').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});


//PHOTOREPORTS

$('.photoreports').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});

//BLOGS
$('.blogs').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});


//VIDEOREPORTS
$('.videointerviews').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});

//TOP MATERIALS
$('.top-materials').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});

//RELATED MATERIALS
$('.related-materials').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});

//SEARCHED MATERIALS
$('.search-section').masonry({
  // options
  columnWidth: '.material-sizer',
  itemSelector: '.news-item',
  isFitWidth: true
});
