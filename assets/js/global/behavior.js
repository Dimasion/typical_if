// $(body).addClass('body--loading');
$(window).load(function() {
    $(".loader").css('display', 'none');
    $(".main-wrap").addClass('main-wrap--loaded');
    $(".header").addClass('header--loaded');
    $(".search-wrap").fadeIn();
    $(".footer").addClass('footer--loaded');
});
