
// |----------------------------------------------------|
// |----------------- OVERLAY EVENTS -------------------|
// |----------------------------------------------------|
$(overlay).click(function() {
    $(this).removeClass(overlayIsActive);
    //MENU CHANGES
    if ($(nav).hasClass(navIsOpened)) {
        if ($(search).hasClass(searchIsClosed)) {
            $(nav).removeClass(navIsOpened);
            moveSearch();

        } else {
            $(this).addClass(overlayIsActive);
            setTimeout(function() {
                moveSearch();
            }, transNormal);
        }
    }

    //SEARCH CHANGES
    closeSearch();
});
