$(document).ready(function() {

    $('.modal-ajax').modaal({
        type: 'ajax'
    });

});


//HEADER

//WHEN SCROLLING
if ($(window).width() > 1170) {
    var headerModifier = "modal-header-wrap--scrolled",
        headerModifier2 = "modal-header-wrap--hidden",
        containerHeight = $(".modal-container").height() + 80;
        header = $(".modal-header-wrap"),

        //NAVIGATION
        nextBtn = $('.navigation-next-wrap'),
        prevBtn = $('.navigation-prev-wrap'),
        //modifiers
        nextBtnMod = 'navigation-next-wrap--hidden',
        prevBtnMod = 'navigation-prev-wrap--hidden';

    $('.modaal-wrapper').on('scroll', function() {
         if ((this.scrollTop > 100)&&(this.scrollTop < containerHeight)) {
           $(header).addClass(headerModifier);
           $(header).removeClass(headerModifier2);

           //navigation
           nextBtn.removeClass(nextBtnMod);
           prevBtn.removeClass(prevBtnMod);
         }
         if (this.scrollTop > containerHeight) {
           $(header).addClass(headerModifier2);
           nextBtn.addClass(nextBtnMod);
           prevBtn.addClass(prevBtnMod);
         }
         if (this.scrollTop < 100) {
             $(header).removeClass(headerModifier);
         }
    });

    // //WHEN CONTAINER IS OUT OF VIEW
    // function Utils() {
    //
    // }
    //
    // Utils.prototype = {
    //     constructor: Utils,
    //     isElementInView: function (element, fullyInView) {
    //         var pageTop = $(window).scrollTop();
    //         var pageBottom = pageTop + $(window).height();
    //         var elementTop = $(element).offset().top;
    //         var elementBottom = elementTop + $(element).height();
    //
    //         if (fullyInView === true) {
    //             return ((pageTop < elementTop) && (pageBottom > elementBottom));
    //         } else {
    //             return ((elementBottom <= pageBottom) && (elementTop >= pageTop));
    //         }
    //     }
    // };
    //
    // var Utils = new Utils();

}
