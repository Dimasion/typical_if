// |----------------------------------------------------|
// |----------------- SEARCH FUNCTIONS  ----------------|
// |----------------------------------------------------|

function openSearch() {
    $(search).removeClass(searchIsClosed);
    setTimeout(function() {
        $(searchForm).fadeIn(transNormal);
        $(searchInput).focus();
    }, transNormal);
    if ($(overlay).hasClass(overlayIsActive) && (!$(nav).hasClass(navIsOpened))) {
        $(overlay).removeClass(overlayIsActive);
    } else {
        $(overlay).addClass(overlayIsActiveUp);
    }
}

function closeSearch() {

    $(search).removeClass(searchIsMoved).addClass(searchIsClosed);
    $(searchInput).val('');
    $(searchForm).fadeOut(transNormal);
    $(overlay).removeClass(overlayIsActiveUp);
}

// |MOVE SEARCH WHEN MENU IS OPENED (<480PX)------------|
function moveSearch() {
    if (sizeWindow < phoneMedia) {
        if ($(search).hasClass(searchIsMoved)) {
            $(search).removeClass(searchIsMoved);
        } else {
            $(search).addClass(searchIsMoved);
        }
    }
}

// |----------------------------------------------------|
// |----------------- SEARCH EVENTS  -------------------|
// |----------------------------------------------------|
$(search).click(function() {
    if ($(nav).hasClass(navIsOpened)) {
        $(this).removeClass(searchIsMoved);
        $(overlay).addClass(overlayIsActiveUp);
        setTimeout(function() {
            openSearch();
        }, transNormal);
    } else {
        $(overlay).removeClass(overlayIsActive);
        openSearch();
    }
});
