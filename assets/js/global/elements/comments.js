// |----------------------------------------------------|
// |----------------- COMMENTS FUNCTIONS ---------------|
// |----------------------------------------------------|
function likeColor() {
    var countInt = parseInt(likeCount.html());

    if (countInt > 0) {
        likeIcon.removeClass(likeIsNegative).addClass(likeIsPossitive);
    } else if (countInt < 0) {
        likeIcon.removeClass(likeIsPossitive).addClass(likeIsNegative);
    } else {
        likeIcon.removeClass(likeIsPossitive).removeClass(likeIsNegative);
    }
}

function dislike() {
    var countInt = parseInt(likeCount.html());
    countInt -= 1;
    likeCount.html(countInt);
    likeColor();
}
function like() {
    var countInt = parseInt(likeCount.html());
    countInt += 1;
    likeCount.html(countInt);
    likeColor();
}

// |----------------------------------------------------|
// |----------------- COMMENTS EVENTS ------------------|
// |----------------------------------------------------|
likeColor();

likePlus.on('click', function () {
    like();
});
likeMinus.on('click', function () {
    dislike();
});



// |----------------------------------------------------|
// |----------------- TEXTAREA DISABLED ----------------|
// |----------------------------------------------------|
if (auth.css('display') != 'block') {
    commentTextArea.attr("disabled", "disabled");
}
