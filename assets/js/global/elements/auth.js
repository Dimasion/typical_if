// |----------------------------------------------------|
// |----------------- COMMENTS FUNCTIONS ---------------|
// |----------------------------------------------------|
function socBoxHoverInModal() {
    if (sizeWindow >= screenMedia) {
        $(socTextBoxModal).addClass(socTextBoxHover);
        setTimeout(function() {
            $(socVkModal).addClass(socVkMoved);
            $(socFbModal).addClass(socFbMoved);
        }, transNormal);

        setTimeout(function() {
            $(socTextBoxModal).fadeOut(transVFast);
        }, transVFast);
    }
}

function socBoxHoverOutModal() {
    if (sizeWindow >= screenMedia) {
        setTimeout(function() {
            $(socTextBoxModal).fadeIn(transVFast);
            $(socTextBoxModal).removeClass(socTextBoxHover);
        }, transNormal);
        $(socVkModal).removeClass(socVkMoved);
        $(socFbModal).removeClass(socFbMoved);
    }
}

// |----------------------------------------------------|
// |----------------- FOOTER EVENTS --------------------|
// |----------------------------------------------------|
$(socBoxModal).hover(function() {
    socBoxHoverInModal();
}, function() {
    socBoxHoverOutModal();
});
