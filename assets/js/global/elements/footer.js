// |----------------------------------------------------|
// |----------------- FOOTER FUNCTIONS  ----------------|
// |----------------------------------------------------|
function socBoxHoverIn() {
    if (sizeWindow >= screenMedia) {
        $(socTextBox).addClass(socTextBoxHover);
        setTimeout(function() {
            $(socVk).addClass(socVkMoved);
            $(socFb).addClass(socFbMoved);
        }, transNormal);

        setTimeout(function() {
            $(socTextBox).fadeOut(transVFast);
        }, transVFast);
    }
}

function socBoxHoverOut() {
    if (sizeWindow >= screenMedia) {
        setTimeout(function() {
            $(socTextBox).fadeIn(transVFast);
            $(socTextBox).removeClass(socTextBoxHover);
        }, transNormal);
        $(socVk).removeClass(socVkMoved);
        $(socFb).removeClass(socFbMoved);
    }
}

// |----------------------------------------------------|
// |----------------- FOOTER EVENTS --------------------|
// |----------------------------------------------------|
$(socBox).hover(function() {
    socBoxHoverIn();
}, function() {
    socBoxHoverOut();
});
