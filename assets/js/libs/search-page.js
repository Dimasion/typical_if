//COUNT ITEMS
var newsLength = $('.news-item').length;
$('.search-count').html(newsLength);


//CHANGE DIV
var countNews = parseInt($('.search-count').html());
if (countNews == 0) {
    $('.not-found').fadeIn();
    $('.view-more').fadeOut();
} else {
    $('.not-found').fadeOut();
    $('.view-more').fadeIn();
}
