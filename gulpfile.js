/////////////////////////////////////////////////
/////////////// MY AUTO TASKS ///////////////////
/////////////////////////////////////////////////
var gulp = require('gulp'),

    //Browser server
    browserSync = require('browser-sync').create(),
    connect = require('gulp-connect'),
    livereload = require('gulp-livereload'),

    //HTML
    jade = require('gulp-jade'),
    // htmlclean = require('gulp-htmlclean'),

    //CSS
    cleanCSS = require('gulp-clean-css'),
    //Забирає лишній текст з js (коментарі, т.д.)
    concatCss = require('gulp-concat-css'),
    //Об'єднує CSS файли

    //JS
    fixmyjs = require("gulp-fixmyjs"),
    //Забирає лишній текст з js (коментарі, т.д.)
    concat = require('gulp-concat'),
    //Об'єднує JS файли

    //SASS
    sass = require("gulp-sass"),

    //IMAGES
    svgmin = require('gulp-svgmin'),
    imagemin = require('gulp-imagemin'),
    svgstore = require('gulp-svgstore'),

    //OTHER
    rename = require('gulp-rename'),
    prefix = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    //Мінімізація файлів

    //FONTS
    fontmin = require('gulp-fontmin'),

    inject = require('gulp-inject');
//Вибирає інформацію з декількох файлів та записує в необхідний файл / для того щоб вказати куди саме записати вибрану інформацію слід вказати коментар н-п.: <!-- inject:svg --><!-- endinject -->


/////////////////////////////////////////////////

/////////////////////////////////////////////////
///////////////   VAR CONFIG   //////////////////
/////////////////////////////////////////////////
var config = {
    /*Prefixes
        app__ - application way
        ass__ - assets way
    */

    //application
    app__html: 'app',
    app__html_includes: 'app/html',
    app__css: 'app/css',
    app__js_c: 'app/js',
    app__js_l: 'app/js/libs',
    app__fonts: 'app/fonts/',
    app__img: 'app/img',
    app__svg_img: 'app/img/svg-images/',
    app__svg_out: 'app/img/svg-sprites/',

    //assets
    ass__jade: ['assets/template/*.jade'],
    ass__jade_watch: ['assets/template/**/*.jade'],
    ass__css: 'assets/css/collector.scss',
    ass__css_w: ['assets/css/**/*.scss'],
    ass__js_g: ['assets/js/global/**/*.js'],
    ass__js_p: ['assets/js/pages/*.js'],
    ass__js_l: ['assets/js/libs/*.js'],
    ass__js_s: ['assets/js/single/**/*.js'],
    ass__fonts: ['assets/fonts/*.*'],
    ass__img: ['assets/img/*.*'],
    ass__svg_ico: ['assets/img/svg-icons/*.svg'],
    ass__svg_img: ['assets/img/svg-images/*.svg'],
    ass__svg_sprite: ['assets/img/svg-sprites/*.svg']
};
/////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
////// CSS TASKS ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
gulp.task('sass', function () {
    gulp.src(config.ass__css)
        .pipe(sass())
        .pipe(concatCss("main.css"))
        .pipe(prefix('last 3 versions'))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(gulp.dest(config.app__css))
        .pipe(livereload());
});
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
//====JS TASKS====================================================//
////////////////////////////////////////////////////////////////////
gulp.task('global-scripts', function () {
    return gulp.src(config.ass__js_g)
        .pipe(concat('common.js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(fixmyjs())
        .pipe(gulp.dest(config.app__js_c))
        .pipe(livereload());
});

gulp.task('pages-scripts', function () {
    return gulp.src(config.ass__js_p)
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(fixmyjs())
        .pipe(gulp.dest(config.app__js_c))
        .pipe(livereload());
});

gulp.task('libs-scripts', function () {
    return gulp.src(config.ass__js_l)
        .pipe(gulp.dest(config.app__js_l))
        .pipe(livereload());
});

gulp.task('single-scripts', function () {
    return gulp.src(config.ass__js_s)
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(gulp.dest(config.app__js_c))
        .pipe(livereload());
});
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
////// HTML TASKS //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

gulp.task('templates', function() {
  gulp.src(config.ass__jade)
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest(config.app__html))
    .pipe(livereload());
});


////////////////////////////////////////////////////////////////////
//====FONTS TASKS===================================================================//
////////////////////////////////////////////////////////////////////
gulp.task('fonts', function () {
    gulp.src(config.ass__fonts)
        .pipe(gulp.dest(config.app__fonts))
        .pipe(fontmin())
        .pipe(livereload());
});



////////////////////////////////////////////////////////////////////
////// IMAGES TASKS ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
gulp.task('images', function () {
    gulp.src(config.ass__img)
        .pipe(imagemin({ optimizationLevel: 100 }))
        .pipe(gulp.dest(config.app__img))
        .pipe(livereload());
});

////////////////////////////////////////////////////////////////////
////// SVG IMAGE TASK //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
gulp.task('svg-images', function () {
    gulp.src(config.ass__svg_img)
        .pipe(imagemin({ optimizationLevel: 100 }))
        .pipe(gulp.dest(config.app__svg_img))
        .pipe(livereload());
});



////////////////////////////////////////////////////////////////////
////// SVG ICONS TASK //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
gulp.task('svg-icons', function () {
    var svgs = gulp
        .src(config.ass__svg_ico, { base: 'src/svg' })
        .pipe(svgmin())
        .pipe(rename({prefix: 'icon-'}))
        .pipe(svgstore({
            inlineSvg: true
        }));

    function fileContents(filePath, file) {
        return file.contents.toString();
    }

    return gulp
        .src(config.ass__svg_sprite)
        .pipe(inject(svgs, {
            transform: fileContents
    }))
    .pipe(livereload())
    .pipe(gulp.dest(config.app__svg_out));
});


////////////////////////////////////////////////////////////////////
////// SERVERS /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

//EXTERNAL SERVER - BROWSER SYNC - localhost:3003 for settings
gulp.task('browser-sync', function () {
   browserSync.init({
       server: {
           baseDir: "app",
           index: "main-page.html",
           livereload: true
       }
   });
});
//CONNECT SERVER - FOR LIVE RELOAD
gulp.task('connect', function () {
    connect.server({
        root: 'app',
        port: 3000,
        livereload: true
    });
});

////////////////////////////////////////////////////////////////////
////// CLEAN APP ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

gulp.task('clean', function () {
	return gulp.src(['app/css/*.css', 'app/js/**/*.js', 'app/img/**/*.*'], {read: false})
		.pipe(clean());
});


////////////////////////////////////////////////////////////////////
////// WATCH TASKS /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//IMAGES
    gulp.task('watch-images', function () {
        livereload.listen();
        gulp.watch(config.ass__img, ['images']);
    });

//FONTS
    gulp.task('watch-fonts', function () {
        livereload.listen();
        gulp.watch(config.ass__fonts, ['fonts']);
    });

//CSS
    gulp.task('watch-sass', function () {
        livereload.listen();
        gulp.watch(config.ass__css_w, ['sass']);
    });

//JAVASCRIPT
    gulp.task('watch-js', function () {
        livereload.listen();
        gulp.watch(config.ass__js_g, ['global-scripts']);
        gulp.watch(config.ass__js_p, ['pages-scripts']);
        gulp.watch(config.ass__js_l, ['libs-scripts']);
        gulp.watch(config.ass__js_s, ['single-scripts']);
    });

//HTML
    gulp.task('watch-html', function () {
        livereload.listen();
        gulp.watch(config.ass__html, ['html']);
    });

    gulp.task('watch-jade', function () {
        livereload.listen();
        gulp.watch(config.ass__jade_watch, ['templates']);
    });

    gulp.task('watch-html-includes', function () {
        livereload.listen();
        gulp.watch(config.ass__html_includes, ['html-includes', 'html']);
    });

//SVG IMAGES
    gulp.task('watch-svg-images', function () {
        livereload.listen();
        gulp.watch(config.ass__svg_img, ['svg-images']);
    });

//SVG ICONS
    gulp.task('watch-svg-icons', function () {
        livereload.listen();
        gulp.watch(config.ass__svg_ico, ['svg-icons']);
    });



////// DEFAULT ////////////////////////////////////////////////////////////////////////
gulp.task('default', [
    'fonts', 'sass', 'templates',
    'pages-scripts', 'global-scripts', 'libs-scripts', 'single-scripts', 'images',
    'svg-images', 'svg-icons',
    //watch
    'watch-images', 'watch-fonts', 'watch-sass', 'watch-js', 'watch-html', 'watch-jade', 'watch-svg-images',
    'watch-svg-icons',
    //servers
    'connect', 'browser-sync'
]);
///////////////////////////////////////////////////////////////////////////////////////
