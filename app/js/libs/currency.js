// |----------------------------------------------------|
// |-----------------GET CURRENCY PRIVATBANK  ----------|
// |----------------------------------------------------|

$(function() {
    $.getJSON('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5', function(data) {
        var dataLength = data.length;
        for (i = 0; i < dataLength; i++) {
            if (data[i].ccy == 'EUR') {
                var eurBuy = parseFloat(data[i].buy);
                var eurSale = parseFloat(data[i].sale);
                $('.currency__eur-buy').append("&euro; " + eurBuy.toFixed(2));
                $('.currency__eur-sale').append("/ " + eurSale.toFixed(2));
            }
            if (data[i].ccy == 'USD') {
                var usdBuy = parseFloat(data[i].buy);
                var usdSale = parseFloat(data[i].sale);
                $('.currency__usd-buy').append("$ " + usdBuy.toFixed(2));
                $('.currency__usd-sale').append("/ " + usdSale.toFixed(2));
            }
        }
    });
});
